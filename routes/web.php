<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\ShopifyWebhooksController;
use App\Http\Controllers\SMSTemplatesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [AuthenticateController::class,'index'])->name('index');

Route::get('/token', [AuthenticateController::class,'token'])->name('token');
Route::get('/install', [AuthenticateController::class,'install'])->name('intsall');
Route::post('/create-product-webhook', [ShopifyWebhooksController::class,'createProductWebHook'])->name('create-product-webhook');
Route::post('/order-product-webhook', [ShopifyWebhooksController::class,'orderProductWebHook'])->name('order-product-webhook');
Route::post('/app-uninstall-webhook/{$shop_url}', [ShopifyWebhooksController::class,'appUninstallWebhook'])->name('app-uninstall-webhook');

Route::get('/admin', [AdminController::class,'index'])->name('admin.index');

Route::patch('/sms-update', [SMSTemplatesController::class,'update'])->name('sms-update');
Route::get('/sms-template', [SMSTemplatesController::class,'index'])->name('sms-index');
