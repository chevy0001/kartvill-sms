<?php

namespace App\Http\Controllers;

use App\Models\Shops;
use Illuminate\Http\Request;
use App\Http\Controllers\FunctionsController;
use App\Http\Controllers\AuthenticateController;

class ShopifyWebhooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function webhooks($topic, $address, $format ){
       
        $array = array(
            'webhook' => array( 
                'topic' => $topic,
                'address'=> env('URL').$address,
                'format'=> $format
              )
        );

        
        $access_token = session('access_token');
        $shop_url = session('shop_url');
        //dd( $topic,$address,$format,$access_token,$shop_url);
        
        $func = new FunctionsController;
        $webhooksCreateProduct = $func->shopify_call($access_token, $shop_url,'/admin/api/2022-04/webhooks.json',$array,'POST');
        $webhooksCreateProduct = json_decode($webhooksCreateProduct['response'], true);
    
        //print_r($webhooksCreateProduct); 

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function verify_request($data, $hmac){
        $verify_hmac = base64_encode( hash_hmac('sha256', $data, env('SHARED_SECRET'), true));
        return hash_equals($hmac, $verify_hmac);
    }
     
    public function createProductWebHook(){
            
        $func = new ShopifyWebhooksController;
        $my_hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $response = '';
        $data = file_get_contents('php://input');
        $utf8 = utf8_encode( $data );
        $data_json = json_decode($utf8, true);
        
        $verify_merchant = $func->verify_request($data, $my_hmac);
        
        if($verify_merchant){
            $response = $data_json;      

        }
        else{
            $response = "This is not from Shopify";
        }
        

        //$title = $data_json['variants'][0]['price'];

        
        $shop_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
        $log = fopen($shop_domain . "-create-products-".rand(100,999)."-.json", "w") or die('Cannot open or create this file');
        fwrite($log, json_encode($response) );
        fclose($log);

     }

     
     //Remove utf-8 error chars
     public function repairName($name){
        $err_char='\u00e2\u0080\u0099';
        $name = str_replace($err_char,"'",$name);
        return $name;

     }

     public function orderProductWebHook(){
            
        $func = new ShopifyWebhooksController;
        $my_hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $response = '';
        $data = file_get_contents('php://input');
        $utf8 = utf8_encode( $data );
        $data_json = json_decode($utf8, true);
        
        $verify_merchant = $func->verify_request($data, $my_hmac);
        
        if($verify_merchant){
            $response = $data_json;

            $phone = $data_json['shipping_address']['phone'];
            $message = "Hello ". $data_json['shipping_address']['first_name']." your order number ".$data_json['order_number']." has been placed. Please pay the total amount of PHP". $data_json["total_price"].". Thank you for shopping with Kartvill.";
            $txtbox_api = env('TXTBOX_API_KEY');
            $phone = str_replace(' ', '', $phone);
            // Start TXTBOX API HERE

                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://ws-live.txtbox.com/v1/sms/push",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => array('message' => $message,'number' => $phone),
                CURLOPT_HTTPHEADER => array(
                 //"X-TXTBOX-Auth: $txtbox_api"
                ),
                ));
            
                $response_sms = curl_exec($curl);
                $err = curl_error($curl);
            
                curl_close($curl);
            
                if ($err) {
                $res = "cURL Error #:" . $err;
                } else {
                $res = $response_sms;
                }


    //************************************************** */


        }
        else{
            $response = "This is not from Shopify";
        }
       
        $shop_domain = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
        $log = fopen($shop_domain . "-create-products-".rand(100,999)."-.json", "w") or die('Cannot open or create this file');
        
      foreach($data_json['line_items'] as $product){
    
        $vendor  = $func->repairName(json_encode($product['vendor']));        
        fwrite($log, $vendor );
        fclose($log);
        //Tommorows task, save to database the essential data
    } 
        
    
     }

     public function appUninstallWebhook($shop_url){
            
    //     $func = new ShopifyWebhooksController;
    //     $my_hmac = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
    //     $response = '';
    //     $data = file_get_contents('php://input');
    //     $utf8 = utf8_encode( $data );
    //    // $data_json = json_decode($utf8, true);
    //     $topic_header = $_SERVER['HTTP_X_SHOPIFY_TOPIC'];
    //     $shop_header = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];  
    //     $verify_merchant = $func->verify_request($data, $my_hmac);
        
    //     if($verify_merchant){
    //             $response = $data_json;    
    //              $shop = Shops::where('shop_url',$shop_url)->delete();
    //               //$response->shop_domain = $data_json['shop_domain'];
            
    //               //$res = $data_json['shop_domain'] . ' is successfully deleted from the database';
    //     }
    //     else{
    //         $response = "This is not from Shopify";
    //     }
        

     }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
