<?php

namespace App\Http\Controllers;

use App\Models\Shops;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Contracts\Session\Session;
use App\Http\Controllers\ShopifyWebhooksController;

class AuthenticateController extends Controller
{

    public  $shop_url,$token,$access_token;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $shopify = $_GET;
        if($shopify == null){
            return redirect('/admin');
        }
        $check = Shops::where('shop_url', $shopify['shop'] )->first();
        
        
        if($check == null){
            return redirect("/install?shop=".$shopify['shop']);
            exit();
        }else{
            session(['access_token' => $check->access_token]);
            session(['shop_url' => $check->shop_url]);
            $webhook = new ShopifyWebhooksController;
            $webhook->webhooks('products/create','/create-product-webhook','json');
            $webhook->webhooks('orders/create','/order-product-webhook','json');
            
            
            
            
            return view('index');

        }
        
                
    }

   

    public function token(){

        // require_once('environment.php');
        // require_once("functions.php");
        // require_once("mysql_connect.php");

            // Set variables for our request
            $api_key = env('API_KEY');
            $shared_secret = env('SHARED_SECRET');
            $params = $_GET; // Retrieve all request parameters
            $hmac = $_GET['hmac']; // Retrieve HMAC request parameter

            
            $webhook = new ShopifyWebhooksController;
            

            $params = array_diff_key($params, array('hmac' => '')); // Remove hmac from params
            ksort($params); // Sort params lexographically

            $computed_hmac = hash_hmac('sha256', http_build_query($params), $shared_secret);

            // Use hmac data to check that the response is from Shopify or not
            if (hash_equals($hmac, $computed_hmac)) {

                // Set variables for our request
                $query = array(
                    "client_id" => $api_key, // Your API key
                    "client_secret" => $shared_secret, // Your app credentials (secret key)
                    "code" => $params['code'] // Grab the access key from the URL
                );

                // Generate access token URL
                $access_token_url = "https://" . $params['shop'] . "/admin/oauth/access_token";

                // Configure curl client and execute request
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $access_token_url);
                curl_setopt($ch, CURLOPT_POST, count($query));
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($query));
                $result = curl_exec($ch);
                curl_close($ch);

                // Store the access token
                $result = json_decode($result, true);
                $access_token = $result['access_token'];

                // Show the access token (don't do this in production!)
                //echo $access_token;


                Shops::create([
                    'shop_url'=>$params['shop'],
                    'access_token'=>$access_token,
                ]);

               
                $webhook->webhooks('app/uninstalled','/app-uninstall-webhook/'.$params['shop'],'json');
                return redirect("https://".$params['shop'] . "/admin/apps/kartvill-sms");

        }
    }   

    public function install(){
        $shop = $_GET['shop'];
        $api_key = env('API_KEY');
        $scopes = env('SCOPES');
        $redirect_uri = env('URL').'/token';

        // Build install/approval URL to redirect to
        $install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

        // Redirect
        return redirect($install_url);
        
    }

    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
