<?php

namespace App\Http\Controllers;

use App\Models\Shops;
use Illuminate\Http\Request;
use App\Http\Controllers\FunctionsController;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         $access = Shops::where('shop_url', 'market-spays.myshopify.com' )->first();

        $func = new FunctionsController;
        $products =$func->shopify_call($access->access_token, $access->shop_url,'/admin/api/2022-04/orders.json', array(), 'GET');
        $orders = json_decode($products['response'], JSON_PRETTY_PRINT);
        
        //dd($products);
        //print_r($products);

        $path = storage_path() . "/json/test.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        $json = json_decode(file_get_contents($path), JSON_PRETTY_PRINT);    

        

      foreach($json['line_items'] as $p){
        $vendor = $p['vendor']; 
        $test_name = $vendor;
        $name = str_replace("\u00e2\u0080\u0099","'",$test_name);
        print_r($vendor);
      }  
      
        //$vendor = $json['line_items']['1']['vendor'];
        
       

        return view('admin.index',compact('orders','name','json'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
