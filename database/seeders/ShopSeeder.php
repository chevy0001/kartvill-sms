<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shops')->insert([
            'shop_url' => 'kartvill-sms.myshopify.com',
            'access_token' => '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',
        ],

        [
            'shop_url' => 'market-spays.myshopify.com',
            'access_token' => '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',
        ],
    );
    }
}
