@extends('layouts.app')

@section('content')

<div class="container">
    <div class="pt-3">
        <div class="row">
            <div class="col-sm">
                Template Name
            </div>
            <div class="col-sm-3">
                SMS Content
            </div>
            <div class="col-sm">
                Enabled
            </div>
            <div class="col-sm">
                Status
            </div>
            <div class="col-sm">
                Action
            </div>
        </div>

        @foreach ($sms_templates as $template )
        <div class="row">
            <div class="col-sm">
                {{ $template->template_name ?? '' }}
            </div>
            <div class="col-sm-3">
                {{ $template->sms_content ?? '' }}
            </div>
            <div class="col-sm">
                 {{ $template->enabled ? 'Yes' : 'No' }}
            </div>
            <div class="col-sm">
                {{ $template->status ? 'Approved' : 'Pending' }}
            </div>
            <div class="col-sm">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editTemplate" data-whatever="{{ $template->id }}">Edit</button>
            </div>
        </div>
        @endforeach
        


    </div>
</div>

<div class="modal fade" id="editTemplate" tabindex="-1" role="dialog" aria-labelledby="editTemplateLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editTemplateLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="/sms-update/1">
            @csrf
            @method('PUT')
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
            <input type="hidden" class="form-control" id="id">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message Template</label>
            <textarea class="form-control" id="sms_content"></textarea>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="Submit" class="btn btn-primary">Edit</button>
     </div> 
    </form>
      
    </div>
  </div>
</div>

<script>
    $('#editTemplate').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body input').val(recipient)
})
</script>


@endsection