@extends('layouts.base')
@section('core')
    @include('layouts.navbar')
    <main>
        @yield('content')
    </main>
@endsection
